// eslint-disable-next-line no-undef
module.exports = {
  description: "Feature Generator",
  prompts: [
    {
      type: "input",
      name: "name",
      message: "feature name",
    },
  ],
  actions: [
    {
      type: "add",
      path: "src/features/{{name}}/api/index.ts",
    },
    {
      type: "add",
      path: "src/features/{{name}}/routes/index.ts",
    },
    {
      type: "add",
      path: "src/features/{{name}}/types/index.ts",
    },
    {
      type: "add",
      path: "src/features/{{name}}/components/index.ts",
    },
    {
      type: "add",
      path: "src/features/{{name}}/assets/index.ts",
    },
    {
      type: "add",
      path: "src/features/{{name}}/hooks/index.ts",
    },
    {
      type: "add",
      path: "src/features/{{name}}/stores/index.ts",
    },
    {
      type: "add",
      path: "src/features/{{name}}/utils/index.ts",
    },
    {
      type: "add",
      path: "src/features/{{name}}/index.ts",
      templateFile: "generators/feature/index.ts.hbs",
    },
  ],
};
