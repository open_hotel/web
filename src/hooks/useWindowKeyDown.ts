import { useEffect } from "react";
type primitive = string | number | boolean | object;
export const useWindowKeyDown = (
  callback: (event: KeyboardEvent) => void,
  options?: {
    capture?: boolean;
    passive?: boolean;
  },
  ...deps: primitive[]
): void => {
  useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent) => {
      callback(event);
    };

    window.addEventListener("keydown", handleKeyDown, options);

    return () => {
      window.removeEventListener("keydown", handleKeyDown, options);
    };
  }, [callback, options, ...deps]);
};
