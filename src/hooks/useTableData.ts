import { getList } from "@/api/axios";
import { TReservation } from "@/features/Reservation";
import { ForEndpoint, Key } from "@/types";
import { useCallback, useState } from "react";
import useSWR from "swr";

export const useTableData = <T, F = void>({
  key,
  initialParams,
}: ForEndpoint<T, F>) => {
  const [params, setParams] = useState(initialParams || ({} as Key<T> | F));
  const options = useCallback(
    () => getList<TReservation>({ params }),
    [params],
  );

  const { data } = useSWR(key, options(), { suspense: true });
  return { data: data || [], params, setParams };
};
