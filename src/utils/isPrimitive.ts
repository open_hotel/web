export const isPrimitive = (val: unknown) => {
  if (val === null) {
    return true;
  }
  return !(typeof val == "object" || typeof val == "function");
};
