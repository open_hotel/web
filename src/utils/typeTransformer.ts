export const typeTransformer = <T, R>(o: T): R => {
  return o as unknown as R;
};
