export { Body } from "./Body";
export { Header } from "./Header";
export { Pagination } from "./Pagination";
export { Structure } from "./Structure";
export type { TableProps, TableColumn } from "./types";
