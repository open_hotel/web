import { TableColumn, TableProps } from "@/components/Table/types";
import { For } from "@/components/Utils/For";
import { RowField } from "@/components/Table/Body/RowField";
import { BaseEntity } from "@/types";

export const Row =
  <T extends BaseEntity>({ columns }: Omit<TableProps<T>, "data">) =>
  (entry: T) => {
    const { id } = entry;
    return (
      <tr key={id}>
        <For<TableColumn<T>> each={columns} render={RowField(entry)} />
      </tr>
    );
  };
