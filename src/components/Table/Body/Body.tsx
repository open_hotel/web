import { TableProps } from "@/components/Table";
import { For } from "@/components/Utils/For";
import { Row } from "@/components/Table/Body/Row";
import { If } from "@/components/Utils/If";
import { BaseEntity } from "@/types";

export function Body<T extends BaseEntity>({
  key,
  data,
  columns,
  children,
}: TableProps<T>) {
  return (
    <tbody>
      <tr key={`filters${key}`}>{children}</tr>
      <If isTrue={data.length === 0}>
        <tr>
          <td
            colSpan={columns.length}
            className={
              "px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900 text-center"
            }
          >
            {"No Entries Found"}
          </td>
        </tr>
      </If>
      <If isTrue={data.length > 0}>
        <For<T> each={data} render={Row({ columns })} />
      </If>
    </tbody>
  );
}
