import { TableColumn } from "@/components/Table/types";
import { BaseEntity } from "@/types";
import { Show } from "@/components/Utils/Show";

export const RowField =
  <T extends BaseEntity>(entry: T) =>
  ({ cell, field }: TableColumn<T>) =>
    (
      <td
        key={`${String(field)}${entry.id}`}
        className={
          "px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900"
        }
      >
        <Show>
          <Show.When isTrue={!!cell}>{cell && cell(entry)}</Show.When>
          <Show.Else>{entry[field] ? String(entry[field]) : "-"}</Show.Else>
        </Show>
      </td>
    );
