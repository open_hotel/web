import { TableProps } from "@/components/Table/types";
import { Body, Header } from "@/components/Table";
import { BaseEntity } from "@/types";

export const Structure = <T extends BaseEntity>({
  data,
  columns,
  children,
  actionHeader,
}: TableProps<T>) => {
  return (
    <>
      <div className={"-my-2 sm:-mx-6 lg:-mx-8"}>
        <div
          className={"py-2 align-middle inline-block min-w-fit sm:px-6 lg:px-8"}
        >
          <div className={"shadow overflow-hidden border-b border-gray-200"}>
            <table className={"divide-y divide-gray-200"}>
              <Header<T> columns={columns}>{actionHeader}</Header>
              <Body<T> columns={columns} data={data}>
                {children}
              </Body>
            </table>
          </div>
        </div>
      </div>
    </>
  );
};
