import { TableColumn } from "@/components/Table/types";
import { BaseEntity } from "@/types";

export const Column = <T extends BaseEntity>({ title }: TableColumn<T>) => (
  <th
    key={title}
    scope={"col"}
    className={
      "px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider flex-1"
    }
  >
    {title}
  </th>
);
