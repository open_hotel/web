import { For } from "@/components/Utils/For";
import { TableColumn } from "@/components/Table";
import { Column } from "@/components/Table/Header/Column";
import { BaseEntity } from "@/types";
import { ReactNode } from "react";
export type HeaderProps<Entry extends BaseEntity> = {
  columns: TableColumn<Entry>[];
  children?: ReactNode;
};
export const Header = <T extends BaseEntity>({ columns }: HeaderProps<T>) => (
  <thead className={"bg-gray-50"}>
    <tr>
      <For<TableColumn<T>> each={columns} render={Column} />
    </tr>
  </thead>
);
