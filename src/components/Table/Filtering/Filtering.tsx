import { FC, ReactNode } from "react";

export type FilteringProps = {
  children: ReactNode;
};

export const Filtering: FC<FilteringProps> = ({ children }) => (
  <>
    <h1>{"Filtering"}</h1>
    {children}
  </>
);
