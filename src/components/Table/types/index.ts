import { BaseEntity, Key } from "@/types";
import { ReactNode } from "react";

export type TableColumn<TEntry extends BaseEntity, TSpecialField = never> = {
  title: string;
  field: Key<TEntry> | TSpecialField;
  cell?: (entry: TEntry) => ReactNode;
};

export type TableProps<TEntry extends BaseEntity, TSpecialField = never> = {
  key?: string;
  data: TEntry[];
  columns: TableColumn<TEntry, TSpecialField>[];
  children?: ReactNode;
  actionHeader?: ReactNode;
};
