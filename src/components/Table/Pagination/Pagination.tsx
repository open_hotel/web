import { FC, ReactNode } from "react";

export type PaginationProps = {
  children: ReactNode;
};

export const Pagination: FC<PaginationProps> = ({ children }) => (
  <>
    <h1>{"Pagination"}</h1>
    {children}
  </>
);
