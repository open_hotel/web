import { FC, ReactNode } from "react";

export type DateProps = {
  children: ReactNode;
};

export const Date: FC<DateProps> = ({ children }) => (
  <>
    <h1>{"Date"}</h1>
    {children}
  </>
);
