import { FC } from "react";
import { FieldWrapperProps } from "@/components/Input/DateSelector/FieldWrapper";
import { If } from "@/components/Utils/If";

export const FieldWrapper: FC<FieldWrapperProps> = ({
  htmlFor,
  errorMessage,
  label,
  children,
}) => (
  <>
    <label
      htmlFor={htmlFor}
      className={
        "block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300"
      }
    >
      {label}
    </label>
    {children}
    <If isTrue={!!errorMessage}>
      <p
        className={"mt-1 text-sm text-gray-500 dark:text-gray-300"}
        id={"file_input_help"}
      >
        {errorMessage}
      </p>
    </If>
  </>
);
