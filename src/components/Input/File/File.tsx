import { FC } from "react";
import { FieldWrapperPassThroughProps } from "@/components/Input/DateSelector/FieldWrapper";
import { FieldErrors, UseFormRegisterReturn } from "react-hook-form";
import { FieldWrapper } from "./FieldWrapper";

export type FileProps = FieldWrapperPassThroughProps & {
  registration: UseFormRegisterReturn;
  errors: FieldErrors;
};

export const File: FC<FileProps> = ({ registration, errors, label }) => (
  <FieldWrapper
    errorMessage={errors[registration.name]?.message}
    htmlFor={registration.name}
    label={label}
  >
    <input
      id={registration.name}
      {...registration}
      type={"file"}
      className={
        "block mb-5 w-full text-xs text-gray-900 bg-gray-50 rounded-lg border border-gray-300 cursor-pointer dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400"
      }
    />
  </FieldWrapper>
);
