import { FieldErrorsImpl, UseFormRegisterReturn } from "react-hook-form";
import { FieldWrapper, FieldWrapperPassThroughProps } from "./FieldWrapper";
import { FC } from "react";

type InputFieldProps = FieldWrapperPassThroughProps & {
  type?: "text" | "email" | "password" | "number";
  className?: string;
  registration: UseFormRegisterReturn;
  errors: FieldErrorsImpl;
};

export const Generic: FC<InputFieldProps> = ({
  type = "text",
  label,
  registration,
  errors,
}: InputFieldProps) => {
  return (
    <FieldWrapper
      errorMessage={errors[registration?.name]?.message}
      htmlFor={registration.name}
      label={label}
    >
      <input
        id={registration.name}
        type={type}
        className={
          "block p-2 w-full text-gray-900 bg-gray-50 rounded-lg border border-gray-300 sm:text-xs focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
        }
        {...registration}
        placeholder={" "}
      />
    </FieldWrapper>
  );
};
