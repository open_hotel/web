import { ErrorFieldWrap } from "@/components/Input/Generic/FieldWrapper";
import * as React from "react";
import { If } from "@/components/Utils/If";

export type FieldWrapperProps = {
  label: string;
  children: React.ReactNode;
  errorMessage?: ErrorFieldWrap;
  htmlFor?: string;
};

export const FieldWrapper = (props: FieldWrapperProps) => {
  const { label, errorMessage, htmlFor, children } = props;
  return (
    <div>
      <div>
        <label
          className={"inline-block text-sm text-gray-900 dark:text-gray-400"}
          htmlFor={htmlFor}
        >
          {label}
        </label>
        {children}
      </div>
      <If isTrue={!!errorMessage}>
        <p
          className={"mt-2 text-xs text-red-600 dark:text-red-400"}
          id={`${htmlFor}_error_help`}
        >
          {errorMessage}
        </p>
      </If>
    </div>
  );
};
