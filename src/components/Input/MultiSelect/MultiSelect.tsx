import { FC, ReactNode } from "react";
import { FieldWrapperPassThroughProps } from "@/components/Input/DateSelector/FieldWrapper";
import { FieldErrorsImpl, UseFormRegisterReturn } from "react-hook-form";
import { OptionType } from "@/components/Input/Select";
import { Option } from "@/components/Input/Select/Option";
import { For } from "@/components/Utils/For";
import { FieldWrapper } from "@/components/Input/MultiSelect/FieldWrapper";

type MultiSelectProps = FieldWrapperPassThroughProps & {
  className?: string;
  registration: UseFormRegisterReturn;
  options: OptionType[];
  value?: never | null;
  placeholder: string;
  required?: boolean;
  render?: (option: OptionType, index?: number) => ReactNode;
  errors: FieldErrorsImpl;
};
export const MultiSelect: FC<MultiSelectProps> = ({
  label,
  placeholder,
  registration,
  options,
  required = false,
  render = ({ text }: OptionType) => text,
  errors,
}: MultiSelectProps) => {
  const renderOption = (item: OptionType, index?: number): ReactNode => (
    <Option {...item}>{render(item, index)}</Option>
  );
  return (
    <FieldWrapper
      errorMessage={errors[registration.name]?.message}
      label={`${label}${required ? "*" : ""}`}
    >
      <select
        autoComplete={"off"}
        multiple
        placeholder={placeholder}
        className={
          "bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
        }
        {...registration}
      >
        <For each={options} render={renderOption} />
      </select>
    </FieldWrapper>
  );
};
