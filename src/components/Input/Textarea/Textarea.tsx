import { FC, ReactNode } from "react";

export type TextareaProps = {
  children: ReactNode;
};

export const Textarea: FC<TextareaProps> = ({ children }) => (
  <>
    <h1>{"Textarea"}</h1>
    {children}
  </>
);
