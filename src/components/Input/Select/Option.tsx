import { FC, ReactNode } from "react";
import { OptionType } from "@/components/Input/Select/index";
type OptionProps = Omit<OptionType, "text"> & { children?: ReactNode };

export const Option: FC<OptionProps> = ({ children, value }: OptionProps) => {
  return <option value={value}>{children}</option>;
};
