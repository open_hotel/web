export * from "./Select";

export type OptionType = {
  text: string;
  value: number | string;
};
