import { FC, ReactNode } from "react";
import { FieldWrapperPassThroughProps } from "@/components/Input/DateSelector/FieldWrapper";
import { FieldWrapper } from "./FieldWrapper";

import { FieldErrorsImpl, UseFormRegisterReturn } from "react-hook-form";
import { OptionType } from "@/components/Input/Select";
import { Option } from "@/components/Input/Select/Option";
import { For } from "@/components/Utils/For";

type SelectProps = FieldWrapperPassThroughProps & {
  className?: string;
  registration: UseFormRegisterReturn;
  options: OptionType[];
  value?: never;
  placeholder: string;
  required?: boolean;
  render?: (option: OptionType, index?: number) => ReactNode;
  errors: FieldErrorsImpl;
};
export const Select: FC<SelectProps> = ({
  value = undefined,
  label,
  registration,
  options,
  placeholder,
  required = false,
  render = ({ text }: OptionType) => text,
  errors,
}: SelectProps) => {
  const renderOption = (item: OptionType, index?: number): ReactNode => (
    <Option {...item}>{render(item, index)}</Option>
  );
  return (
    <FieldWrapper
      errorMessage={errors[registration.name]?.message}
      label={`${label}${required ? "*" : ""}`}
    >
      <select
        className={`block p-2 mb-6 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500`}
        placeholder={placeholder}
        value={value}
        {...registration}
      >
        <option value={undefined}>{label}</option>
        <For each={options} render={renderOption} />
      </select>
    </FieldWrapper>
  );
};
