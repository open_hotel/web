import { FC } from "react";
import {
  FieldWrapper,
  FieldWrapperPassThroughProps,
} from "@/components/Input/DateSelector/FieldWrapper";
import { FieldErrorsImpl, UseFormRegisterReturn } from "react-hook-form";

type DateSelectorProps = FieldWrapperPassThroughProps & {
  type?: "text" | "email" | "password" | "number";
  registration: UseFormRegisterReturn;
  placeholder: string;
  errors: FieldErrorsImpl;
};

export const DateSelector: FC<DateSelectorProps> = ({
  label,
  registration,
  placeholder,
  errors,
}: DateSelectorProps) => {
  return (
    <FieldWrapper
      errorMessage={errors[registration.name]?.message}
      htmlFor={registration.name}
      label={label}
    >
      <input
        id={registration.name}
        type={"date"}
        {...registration}
        placeholder={placeholder}
        className={
          "bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
        }
      />
    </FieldWrapper>
  );
};
