import { ErrorFieldWrap } from "@/components/Input/Generic/FieldWrapper";
import * as React from "react";
import { If } from "@/components/Utils/If";
import { ReactComponent as Date } from "@/assets/date.svg";
export type FieldWrapperProps = {
  label: string;
  children: React.ReactNode;
  errorMessage?: ErrorFieldWrap;
  htmlFor?: string;
};

export type FieldWrapperPassThroughProps = Omit<FieldWrapperProps, "children">;

export const FieldWrapper = (props: FieldWrapperProps) => {
  const { label, errorMessage, htmlFor, children } = props;
  return (
    <div>
      <label
        htmlFor={htmlFor}
        className={
          "block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300"
        }
      >
        {label}
      </label>
      <div className={"relative"}>
        <div
          className={
            "absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none"
          }
        >
          <Date />
        </div>
        {children}
      </div>
      <If isTrue={!!errorMessage}>
        <p
          className={"mt-2 text-xs text-red-600 dark:text-red-400"}
          id={`${htmlFor}_error_help`}
        >
          {errorMessage}
        </p>
      </If>
    </div>
  );
};
