import { FC } from "react";

export const General: FC = () => (
  <div
    className={"w-screen h-screen flex items-center justify-center bg-gray-800"}
  >
    <div
      className={
        "inline-block text-gray-800 align-center bg-gradient-to-r from-indigo-500 via-purple-500 to-pink-500 p-1 hover:p-2 rounded-lg"
      }
    >
      {"Cargando22..."}
    </div>
  </div>
);
