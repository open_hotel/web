import { ReactElement, ReactNode, ReactPortal } from "react";
import { typeTransformer } from "@/utils";

export type IfProps = {
  isTrue: boolean;
  children: ReactNode;
};
type ReturnType =
  | IfReturnType
  | Iterable<ReactNode>
  | number
  | boolean
  | undefined;
type IfReturnType = ReactElement | ReactPortal | null;
export const If = ({ isTrue, children }: IfProps) =>
  typeTransformer<ReturnType, IfReturnType>(isTrue ? children : null);
