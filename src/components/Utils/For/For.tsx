import { Children, ReactNode } from "react";
export type ForProps<T> = {
  render: (item: T, index?: number) => ReactNode;
  each: Array<T>;
};

export const For = <T,>({ render, each }: ForProps<T>) => (
  <>{Children.toArray(each.map((item, index) => render(item, index)))}</>
);
