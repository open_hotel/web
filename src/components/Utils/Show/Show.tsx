import {
  ReactNode,
  Children,
  ReactElement,
  ReactFragment,
  ReactPortal,
} from "react";
import { If } from "../If";
import { isPrimitive, typeTransformer } from "@/utils";
type ShowProps = {
  children: ReactNode;
};
type NullReactNode = null | ReactNode;
type VanillaChildren =
  | ReactElement
  | string
  | number
  | ReactFragment
  | ReactPortal
  | boolean
  | null
  | undefined;
type SuperChildren = ReactElement | ReactPortal;
export const Show = (props: ShowProps) => {
  let when: NullReactNode = null;
  let otherwise: NullReactNode = null;

  Children.forEach(props.children, (children) => {
    if (!children || isPrimitive(children)) return;
    const superChildren = typeTransformer<VanillaChildren, SuperChildren>(
      children,
    );
    if (superChildren.props.isTrue === undefined) {
      otherwise = children;
    } else if (!when && superChildren.props.isTrue) {
      when = children;
    }
  });

  return when || otherwise;
};

Show.When = If;
Show.Else = ({ children }: ShowProps) =>
  typeTransformer<VanillaChildren, SuperChildren>(children);
