import { FC } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { TestPage } from "@/features/test";
import { Reservation } from "@/features/Reservation";
import { SWRConfig } from "swr";

const App: FC = () => {
  return (
    <SWRConfig>
      <BrowserRouter>
        <Routes>
          <Route element={<p>{"Hola"}</p>} path={"/"} />
          <Route element={<TestPage />} path={"/test"} />
          <Route element={<Reservation.List />} path={"/reservation"} />
          <Route element={<Reservation.New />} path={"/reservation/new"} />
          <Route element={<Reservation.Edit />} path={"/reservation/:id"} />
        </Routes>
      </BrowserRouter>
    </SWRConfig>
  );
};

export default App;
