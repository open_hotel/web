import { createContext, ReactNode, useContext, useMemo, Context } from "react";

export type FormDataContextType<T> = {
  data: T;
};
export function createFormDataContext<T>() {
  return createContext<FormDataContextType<T>>({} as FormDataContextType<T>);
}
export function createFormData<T>() {
  const FormDataContext = createFormDataContext<T>(); // We type the Context based on the generic AuthService
  const useDataForm = createUseDataForm<T>(FormDataContext);
  const FormDataProvider = createFormDataProvider(FormDataContext);
  return {
    FormDataContext,
    FormDataProvider,
    useDataForm,
  };
}

// Export the provider as we need to wrap the entire app with it
export function createFormDataProvider<T>(
  ContextProp: Context<FormDataContextType<T>>,
) {
  return ({
    children,
    data,
  }: {
    children: ReactNode;
    data: T;
  }): JSX.Element => {
    const memoedValue = useMemo(() => ({ data }), [data]);

    // We only want to render the underlying app after we
    // assert for the presence of a current user.
    return (
      <ContextProp.Provider value={memoedValue}>
        {children}
      </ContextProp.Provider>
    );
  };
}

// Let's only export the `useAuth` hook instead of the context.
// We only want to use the hook directly and never the context component.
export function createUseDataForm<T>(
  ContextProp: Context<FormDataContextType<T>>,
) {
  return () => useContext(ContextProp);
}
