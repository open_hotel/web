import { Key as SWRKey } from "swr";
export interface BaseEntity {
  id?: string;
}

export type ForEndpoint<T, F> = {
  key: SWRKey;
  initialParams?: Key<T> | F;
};

export type Key<Entry> = keyof Entry;
