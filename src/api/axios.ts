import { API_URL } from "@/config";
import axios, { AxiosRequestConfig } from "axios";
const instance = axios.create({
  baseURL: API_URL,
});
export const getList =
  <T>(options: AxiosRequestConfig<T>) =>
  (url: string) =>
    instance
      .get<Array<T>>(url, { ...options, method: "get" })
      .then((res) => res.data);
export const getItem =
  <T>(options: AxiosRequestConfig<T>) =>
  (url: string) =>
    instance.get<T>(url, { ...options, method: "get" }).then((res) => res.data);
