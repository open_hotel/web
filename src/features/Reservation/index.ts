import { TReservation } from "./types";
import { List, New, Edit } from "./routes";
const Reservation = { List, Edit, New };
export { TReservation, Reservation };
