import { DateSelector } from "@/components/Input/DateSelector";
import { Generic } from "@/components/Input/Generic";
import { MultiSelect } from "@/components/Input/MultiSelect";
import { OptionType, Select } from "@/components/Input/Select";
import { TReservation } from "@/features/Reservation";
import { useEffect } from "react";
import { useForm } from "react-hook-form";
export type FormProps = {
  data: TReservation;
  onSubmit: (data: TReservation) => void;
};
export const Form = ({ data, onSubmit }: FormProps) => {
  const form = useForm<TReservation>();

  useEffect(() => {
    form.reset(data);
  }, [data]);

  const options: OptionType[] = [
    { text: "option1", value: "option1" },
    { text: "option2", value: "option2" },
    { text: "option3", value: "option3" },
    { text: "option4", value: "option4" },
  ];
  const options2: OptionType[] = [
    { text: "option1", value: 1 },
    { text: "option2", value: 2 },
    { text: "option3", value: 3 },
    { text: "option4", value: 4 },
  ];
  return (
    <form onSubmit={form.handleSubmit(onSubmit)}>
      <Generic
        errors={form.formState.errors}
        label={"mainGuest"}
        registration={form.register("mainGuest")}
        type={"text"}
      />

      <Select
        errors={form.formState.errors}
        label={"payer"}
        options={options}
        placeholder={"Select payer"}
        registration={form.register("payer")}
      />
      <Select
        errors={form.formState.errors}
        label={"holder"}
        options={options}
        placeholder={"Select holder"}
        registration={form.register("holder")}
      />
      <Select
        errors={form.formState.errors}
        label={"state"}
        options={options2}
        placeholder={"Select state"}
        registration={form.register("state")}
      />
      <MultiSelect
        errors={form.formState.errors}
        label={"accommodations"}
        options={options}
        placeholder={"Select accommodations"}
        registration={form.register("accommodations")}
      />
      <DateSelector
        errors={form.formState.errors}
        label={"startDate"}
        placeholder={"Select startDate"}
        registration={form.register("startDate")}
      />
      <DateSelector
        errors={form.formState.errors}
        label={"endDate"}
        placeholder={"Select endDate"}
        registration={form.register("endDate")}
      />
      <Generic
        errors={form.formState.errors}
        label={"observations"}
        registration={form.register("observations")}
        type={"text"}
      />
    </form>
  );
};
