import { Structure } from "@/components/Table";
import { TReservation } from "@/features/Reservation";
import { useTableData } from "@/hooks/useTableData";
import { listKey } from "../keys";
export const Table = () => {
  const { data } = useTableData<TReservation>({ key: "Reservation" });
  return <Structure<TReservation> columns={listKey} data={data} />;
};
