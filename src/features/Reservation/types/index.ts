import { BaseEntity } from "@/types";

export class TReservation implements BaseEntity {
  id?: string;
  accommodations: Array<string>;
  startDate: string;
  endDate: string;
  mainGuest: string;
  payer: string;
  holder: string;
  state: number;
  observations: string;

  constructor(reservation?: TReservation) {
    if (reservation) {
      this.id = reservation.id;
      this.accommodations = reservation.accommodations;
      this.startDate = reservation.startDate;
      this.endDate = reservation.endDate;
      this.mainGuest = reservation.mainGuest;
      this.payer = reservation.payer;
      this.holder = reservation.holder;
      this.state = reservation.state;
      this.observations = reservation.observations;
    } else {
      this.id = "";
      this.accommodations = [];
      this.startDate = "";
      this.endDate = "";
      this.mainGuest = "";
      this.payer = "";
      this.holder = "";
      this.state = 0;
      this.observations = "";
    }
  }
}
