import { TableColumn } from "@/components/Table";
import { TReservation } from "@/features/Reservation";

export const listKey: TableColumn<TReservation>[] = [
  { title: "", field: "id" },
  { title: "Titular", field: "mainGuest" },
  { title: "Pagador", field: "payer" },
  { title: "Proveedor", field: "holder" },
  { title: "Estado", field: "state" },
  { title: "Entrada", field: "startDate" },
  { title: "Salida", field: "endDate" },
  { title: "Observaciones", field: "observations" },
];
