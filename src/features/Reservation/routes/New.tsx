import { TReservation } from "@/features/Reservation";
import { Form } from "../components";
import { axios } from "@/lib/axios";
export const New = () => {
  const onSubmit = async (values: TReservation) => {
    console.log({ values });
    const response = await axios.post("Reservation", values);
    console.log({ response });
  };
  return (
    <div style={{ padding: 20 }}>
      <Form data={{} as TReservation} onSubmit={onSubmit} />
    </div>
  );
};
