import { getItem } from "@/api/axios";
import { TReservation } from "@/features/Reservation";
import { axios } from "@/lib/axios";
import { useCallback } from "react";
import { useParams } from "react-router";
import useSWR from "swr";
import { Form } from "../components";

export const Edit = () => {
  const { id } = useParams();
  const swrOptions = useCallback(
    () => getItem<TReservation>({ params: { id } }),
    [id],
  );

  const { data } = useSWR("Reservation", swrOptions(), { suspense: true });

  const onSubmit = (values: TReservation) => {
    console.log({ values });
    const response = axios.post(`Reservation/${id}`, values);
    console.log({ response });
  };
  return (
    <div style={{ padding: 20 }}>
      {data ? <Form data={data} onSubmit={onSubmit} /> : <></>}
    </div>
  );
};
