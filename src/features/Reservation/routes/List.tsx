import { General } from "@/components/FallbackSuspense";
import { Table } from "../components/Table";
import { Suspense } from "react";
export const List = () => {
  return (
    <div style={{ padding: 20 }}>
      <Suspense fallback={<General />}>
        <Table />
      </Suspense>
    </div>
  );
};
