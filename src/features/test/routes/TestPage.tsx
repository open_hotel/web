import { Generic } from "@/components/Input/Generic";
import { File } from "@/components/Input/File";

import { useForm } from "react-hook-form";
import { OptionType, Select } from "@/components/Input/Select";
import { Structure, TableColumn } from "@/components/Table";
import { BaseEntity } from "@/types";
import { MultiSelect } from "@/components/Input/MultiSelect";
import { DateSelector } from "@/components/Input/DateSelector";
interface Product extends BaseEntity {
  name: string;
}
export const TestPage = () => {
  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm();

  const data: Product[] = [
    { id: "1", name: "Chocolate" },
    { id: "2", name: "Leche" },
  ];
  const columns: TableColumn<Product>[] = [
    { title: "Id", field: "id" },
    { title: "Nombre", field: "name" },
  ];
  const onSubmit = console.log;
  const options: OptionType[] = [
    { text: "Normal", value: 1 },
    { text: "Supreme", value: 2 },
  ];
  return (
    <div style={{ padding: 20 }}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Generic
          errors={errors}
          label={"TestString"}
          registration={register("testString")}
          type={"text"}
        />

        <Generic
          errors={errors}
          label={"TestNumber"}
          registration={register("testNumber")}
          type={"number"}
        />
        <Select
          errors={errors}
          label={"type"}
          options={options}
          placeholder={"Select type"}
          registration={register("type", { required: true })}
        />
        <MultiSelect
          errors={errors}
          label={"type2"}
          options={options}
          placeholder={"Select type2"}
          registration={register("type2", { required: false })}
        />
        <File errors={errors} label={"file"} registration={register("file")} />
        <DateSelector
          errors={errors}
          label={"Fecha"}
          placeholder={"Seleccione fecha"}
          registration={register("fecha")}
        />
        <button type={"submit"}>{"Submit"}</button>
      </form>
      <Structure<Product> columns={columns} data={data} />
    </div>
  );
};
